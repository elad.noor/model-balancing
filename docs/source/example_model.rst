Example Model
=============

Small test models
*****************

We tested **Model Balancing** extensively with three small test models.
Model and data files, as well as results for various estimation scenarios
can be found on our GitHub repository named
`model-balancing <https://github.com/liebermeister/model-balancing>`_.

.. figure:: _static/ecoli_model.svg

    **Figure 1**: Model of *E. coli* central carbon metabolism, taken from Noor et al. :footcite:`nfbd:16`. 

.. figure:: _static/diagnostic_errors.png

    **Figure 2**: Diagnostic plots for fit with original kinetic data. Colors indicate
    relative fitting errors. Blue: fitted value too high. Red: fitted value too
    low. Maximal fold changes (whether up or down) are given by
    numbers. For more examples, see the
    `resources/models folder <https://github.com/liebermeister/model-balancing/tree/master/resources/models>`_.

*E. coli* model
***************

The model of *E. coli* central carbon metabolism from Noor et al. :footcite:`nfbd:16` comprises
40 metabolites and 30 reactions and contains 107 :math:`K_{\rm M}` values and 167 kinetic constants
(:math:`K_{\rm M}` values, as well as forward and backward :math:`k_{\rm cat}` values).  The
model structure (Figure 1) is defined by the file
`e_coli_noor_2016.tsv <https://raw.githubusercontent.com/liebermeister/model-balancing/master/resources/models/e_coli_noor_2016/e_coli_noor_2016.tsv>`_.
*In-vitro* kinetic data had been mapped to the *E. coli*
model in Noor et al. :footcite:`nfbd:16`. Here we used the same data with a few
changes. In addition, we generated a balanced version of the same data set, also
taken from Noor et al. :footcite:`nfbd:16`. For details about the model, see the
documentation on github (numbers of data values model variables
estimated can be found in the report files). 
To model aerobic growth on glucose, we used state data from Noor et al. :footcite:`nfbd:16` which include
measured flux data from van Rijsewijk et al. :footcite:`hann:11`, proteomics data from
Schmidt et al. :footcite:`skva:15`, and metabolomics data from Gerosa et al. :footcite:`gerc:15`. To model
several metabolic states, we used a data set from Davidi et al. :footcite:`dnlb:16`,
where a larger network model had been considered, proteomics data from
different sources were used, and flux data had been computed by FBA.
I linearly the flux data onto the *E. coli* model to obtain
complete and consistent flux values. A comparison between the two data
sets reveals a discrepancy in scaling: the (FBA-derived) fluxes from
Davidi et al. :footcite:`dnlb:16` were smaller than the fluxes taken from Noor et al. :footcite:`nfbd:16`
by an approximate factor of 10, while enzyme concentrations were smaller by an
approximate factor of 2.

References
**********

.. footbibliography::
