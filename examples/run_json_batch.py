import itertools
import os

from path import Path

from model_balancing.batch import run_batch

BASINHOPPING_KWARGS = {"niter": 25, "niter_success": 5, "seed": 2019}

# methods: SLSQP or COBYLA
MINIMIZER_KWARGS = {"method": "SLSQP", "options": {"maxiter": 100, "disp": False}}

INIT_METHODS = ["geom_mean"]
# INIT_METHODS = ["convex", "geom_mean", "true_value"]

ALPHAS = [0.0, 1.0]
# ALPHAS = [1.0]
# ALPHAS = [0.0]

# change to the root path of the model-balancing package
os.chdir(Path(os.path.abspath(__file__)).parent.parent)

models = [
    [
        "branch_point_model",
        "e_coli"
    ],
    ["artificial"],
    [
        "noisefree",
        "noisy"
    ],
    ["state"],
    [
        "no_kinetic",
        "noisefree_Keq",
        "noisefree_kinetic",
        "noisy_Keq",
        "noisy_kinetic"
    ],
]

example_names = map("_".join, itertools.product(*models))

run_batch(
    example_names,
    init_methods=INIT_METHODS,
    alphas=ALPHAS,
    basinhopping_kwargs=BASINHOPPING_KWARGS,
    minimizer_kwargs=MINIMIZER_KWARGS,
)

