import os
import json
from model_balancing.batch import get_convex_solution
from model_balancing.io import read_arguments_from_json
from model_balancing.model_balancing_noncvx import ModelBalancing
from path import Path
import pandas as pd
import numpy as np


BASINHOPPING_KWARGS = {"niter": 10, "niter_success": 5, "seed": 2019}
# BASINHOPPING_KWARGS = None

# methods: SLSQP or COBYLA
MINIMIZER_KWARGS = {"method": "SLSQP", "options": {"maxiter": 1000, "disp":
    False}}

INIT_METHOD = "convex"
#INIT_METHOD = "geom_mean"
#INIT_METHOD = "true_value"
ALPHA = 0.0
BETA = 0.0

BASE_DIR = Path(os.path.abspath(__file__)).parent.parent

# %%-----------------------------------------------------------------------------
# example_name = "e_coli_artificial_noisefree_state_no_kinetic"
example_name = "branch_point_model_artificial_noisefree_state_no_kinetic"
json_fname = BASE_DIR / "examples" / example_name + ".json"

with open(json_fname, "rt") as fp:
    args = read_arguments_from_json(json.load(fp))
args["alpha"] = ALPHA
args["beta"] = BETA
mb = ModelBalancing(**args)

if INIT_METHOD == "convex":
    initial_point = get_convex_solution(args)
    if initial_point is None:
        raise ValueError("Cannot initialize using CVXPY")
    mb._var_dict.update(initial_point)
elif INIT_METHOD == "geom_mean":
    mb._var_dict.update(mb.ln_geom_mean)
elif INIT_METHOD == "true_value":
    mb._var_dict.update(mb.ln_true_value)
else:
    raise KeyError(f"Unknown INIT_METHOD: {INIT_METHOD}")

result_dict = {
    "JSON": example_name,
    "alpha": ALPHA,
    "beta": 0.0,
    "initialization": INIT_METHOD,
    "objective_before_solving": mb.objective_value,
}

result = mb.solve(
    basinhopping_kwargs=BASINHOPPING_KWARGS,
    minimizer_kwargs=MINIMIZER_KWARGS,
)
result_dict["message"] = result.message
print(
    f"solver message = {result.message}, "
    f"optimized total squared Z-scores = {mb.objective_value:.3f}"
)
result_dict.update(mb.get_z_scores())
result_dict["objective_after_solving"] = mb.objective_value

with open(BASE_DIR / "res" / example_name + "_stats.tsv", "wt") as fp:
    fp.write(mb.to_state_sbtab().to_str())
with open(BASE_DIR / "res" / example_name + "_model.tsv", "wt") as fp:
    fp.write(mb.to_model_sbtab().to_str())


for i in range(mb.Nr):
    for j in range(mb.Ncond):
        direction = np.sign(mb.fluxes[i, j])


df = pd.DataFrame([
    mb.thermodynamic_constraints.A @ result.x,
    mb.thermodynamic_constraints.lb,
    mb.thermodynamic_constraints.ub,
    list(mb.fluxes.flat),
    list(mb.driving_forces.flat),
    list(mb.ln_eta_thermodynamic.flat),
], index=["Ax", "lb", "ub", "flux", "driving_force", "ln_eta_th"]).transpose()

df.to_csv(BASE_DIR / "res" / example_name + "_constraints.csv")

print(result_dict)
