[![Documentation Status](https://readthedocs.org/projects/model-balancing/badge/?version=latest)](https://model-balancing.readthedocs.io/en/latest/?badge=latest)

Model balancing
===============

[Model balancing](https://www.metabolic-economics.de/model-balancing/index.html) is a computational method to
determine plausible kinetic constants and metabolic states in kinetic metabolic models. It integrates flux,
metabolite, protein, and kinetic constant data, using prior distributions for all these variables, and
computes the joint posterior mode.
Model balancing can be run in matlab or python. Data tables can be provided in [SBtab](https://www.sbtab.net)
format, models can be provided in  [SBML](http://sbml.org) or  [SBtab](https://www.sbtab.net) format.

## Installation and usage
All details can be found on our [Read The Docs](https://model-balancing.readthedocs.io/en/latest/index.html) page.

If you prefer to use Matlab, we refer you to Wolfram Liebermeister's [model-balancing](https://github.com/liebermeister/model-balancing) package.

## Contact
Please contact [Wolfram Liebermeister](mailto:wolfram.liebermeister@gmail.com)
and [Elad Noor](mailto:elad.noor@weizmann.ac.il) with any questions or comments.

## References
Liebermeister W. and Noor E. (2021), *Model balancing: in search of consistent
metabolic states and in-vivo kinetic constants*
[bioRxiv doi:10.1101/2019.12.23.887166v2](https://www.biorxiv.org/content/10.1101/2019.12.23.887166v2)

[www.metabolic-economics.de/model-balancing/](https://www.metabolic-economics.de/model-balancing/index.html)
