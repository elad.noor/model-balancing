import cvxpy as cp
import pytest

from model_balancing.io import read_adguments_from_filename
from model_balancing.model_balancing_cvx import ModelBalancingConvex


def test_toy_example(allclose):
    json_fname = "tests/toy_example.json"
    args = read_adguments_from_filename(json_fname)

    try:
        mbc = ModelBalancingConvex(**args)
        mbc.initialize_with_gmeans()
        assert mbc.is_gmean_feasible()
        assert mbc.solve(verbose=False) in cp.settings.SOLUTION_PRESENT
    except cp.error.SolverError:
        pytest.skip()

    assert allclose(mbc._var_dict["ln_Keq"].value, [0.71, 3.41, -0.29], atol=0.01)
    assert allclose(
        mbc._var_dict["ln_Km"].value,
        [-8.69, -7.80, -8.45, -12.27, -11.44, -7.02],
        atol=0.01,
    )
    assert allclose(mbc._var_dict["ln_kcatf"].value, [4.27, 6.82, 3.82], atol=0.01)
