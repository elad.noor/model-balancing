import pytest

from model_balancing.io import read_adguments_from_filename
from model_balancing.model_balancing_noncvx import ModelBalancing


def test_toy_example(allclose):
    json_fname = "tests/toy_example.json"
    args = read_adguments_from_filename(json_fname)

    mb = ModelBalancing(**args)
    mb.initialize_with_gmeans()
    assert mb.is_gmean_feasible()
    mb.solve(basinhopping_kwargs={"niter": 4})
    assert allclose(mb._var_dict["ln_Keq"], [0.6, 3.4, -0.4], atol=0.1)
    assert allclose(
        mb._var_dict["ln_Km"], [-8.6, -7.8, -8.4, -12.3, -11.4, -7.0], atol=0.1
    )
    assert allclose(mb._var_dict["ln_kcatf"], [4.2, 6.8, 3.6], atol=0.1)
